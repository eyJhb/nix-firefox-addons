{ stdenv }:

{
  "1password-x-password-manager" = {
    pname = "1password-x-password-manager";
    version = "8.10.62.27";
    addonId = "{d634138d-c276-4fc8-924b-40a0ea21d284}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4439694/1password_x_password_manager-8.10.62.27.xpi";
    sha256 = "5561a53d4af2bcbc8ba8f80ea4f6f712ed9f0f1e7cb839738774c346de22ff90";
    meta = with stdenv.lib; {
      homepage = "https://1password.com";
      license = {
        shortName = "1pwd";
        fullName = "Service Agreement for 1Password users and customers";
        url = "https://1password.com/legal/terms-of-service/";
        free = false;
      };
      platforms = platforms.all;
    };
  };
  "adsum-notabs" = {
    pname = "adsum-notabs";
    version = "1.2resigned1";
    addonId = "{c9f848fb-3fb6-4390-9fc1-e4dd4d1c5122}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4274750/adsum_notabs-1.2resigned1.xpi";
    sha256 = "853674a75add207d3c9635de0a43a478eed8805c0e6b673c1f5f4a26f765eb0e";
    meta = with stdenv.lib; {
      homepage = "https://gitlab.com/adsum/firefox-notabs";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "anchors-reveal" = {
    pname = "anchors-reveal";
    version = "1.2resigned1";
    addonId = "jid1-XX0TcCGBa7GVGw@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4270578/anchors_reveal-1.2resigned1.xpi";
    sha256 = "bcae97368b2c2271cf694676a2fb29dd168698e6e8105a6df6607fcf0bbce77a";
    meta = with stdenv.lib; {
      homepage = "http://dascritch.net/post/2014/06/24/Sniffeur-d-ancre";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "auto-tab-discard" = {
    pname = "auto-tab-discard";
    version = "0.6.7";
    addonId = "{c2c003ee-bd69-42a2-b0e9-6f34222cb046}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4045009/auto_tab_discard-0.6.7.xpi";
    sha256 = "89e59b8603c444258c89a507d7126be52ad7a35e4f7b8cfbca039b746f70b5d5";
    meta = with stdenv.lib; {
      homepage = "https://webextension.org/listing/tab-discard.html";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "bitwarden" = {
    pname = "bitwarden";
    version = "2025.2.0";
    addonId = "{446900e4-71c2-419f-a6a7-df9c091e268b}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4440363/bitwarden_password_manager-2025.2.0.xpi";
    sha256 = "c4d7f355a2269620482f50edac7fce3c19f515190f24cdf80edc865f71d3a374";
    meta = with stdenv.lib; {
      homepage = "https://bitwarden.com";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "browserpass" = {
    pname = "browserpass";
    version = "3.9.0";
    addonId = "browserpass@maximbaz.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4406417/browserpass_ce-3.9.0.xpi";
    sha256 = "514c1a606d7bc82045d21bcbc6e5b1e5007446ab32aa01f0f17fbe0bde03ef0c";
    meta = with stdenv.lib; {
      homepage = "https://github.com/browserpass/browserpass-extension";
      license = licenses.isc;
      platforms = platforms.all;
    };
  };
  "buster-captcha-solver" = {
    pname = "buster-captcha-solver";
    version = "3.1.0";
    addonId = "{e58d3966-3d76-4cd9-8552-1582fbc800c1}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4297951/buster_captcha_solver-3.1.0.xpi";
    sha256 = "6892c4e1777b6e5480bb4224c2503b62d1cb92b49996ee9948cb746110a351d8";
    meta = with stdenv.lib; {
      homepage = "https://github.com/dessant/buster#readme";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "close-other-windows" = {
    pname = "close-other-windows";
    version = "0.2resigned1";
    addonId = "{fab4ea0f-e0d3-4bb4-9515-aea14d709f69}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4271965/close_other_windows-0.2resigned1.xpi";
    sha256 = "98428363daef4acafd983f37125c4c24f1f843c76e801b2fa5ca3a9f71a8db6a";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "cookie-autodelete" = {
    pname = "cookie-autodelete";
    version = "3.8.2";
    addonId = "CookieAutoDelete@kennydo.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4040738/cookie_autodelete-3.8.2.xpi";
    sha256 = "b02438aa5df2a79eb743da1b629b80d8c48114c9d030abb5538b591754e30f74";
    meta = with stdenv.lib; {
      homepage = "https://github.com/Cookie-AutoDelete/Cookie-AutoDelete";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "dark-night-mode" = {
    pname = "dark-night-mode";
    version = "2.0.7";
    addonId = "{27c3c9d8-95cd-44e6-ae9c-ff537348b9f3}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4419518/dark_night_mode-2.0.7.xpi";
    sha256 = "def7cba80f92002b739c59a572eb8aa915084a58db5741bc591d1803a2e23603";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "dark-scroll-for-tweetdeck" = {
    pname = "dark-scroll-for-tweetdeck";
    version = "2.0.0";
    addonId = "{759d3eb8-baf1-49e0-938b-0f963fdac3ae}";
    url = "https://addons.mozilla.org/firefox/downloads/file/1754743/dark_scroll_for_tweetdeck-2.0.0.xpi";
    sha256 = "e0f4e625eda09e9c8300ef650373d5a582a8c77c18eba572aa39d0bd8e3eb596";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.lgpl3;
      platforms = platforms.all;
    };
  };
  "darkreader" = {
    pname = "darkreader";
    version = "4.9.103";
    addonId = "addon@darkreader.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/4439735/darkreader-4.9.103.xpi";
    sha256 = "f565b2263a71626a0310380915b7aef90be8cc6fe16ea43ac1a0846efedc2e4c";
    meta = with stdenv.lib; {
      homepage = "https://darkreader.org/";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "decentraleyes" = {
    pname = "decentraleyes";
    version = "3.0.0";
    addonId = "jid1-BoFifL9Vbdl2zQ@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4392113/decentraleyes-3.0.0.xpi";
    sha256 = "6f2efed90696ac7f8ca7efb8ab308feb3bdf182350b3acfdf4050c09cc02f113";
    meta = with stdenv.lib; {
      homepage = "https://decentraleyes.org";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "disconnect" = {
    pname = "disconnect";
    version = "20.3.1.2";
    addonId = "2.0@disconnect.me";
    url = "https://addons.mozilla.org/firefox/downloads/file/4240055/disconnect-20.3.1.2.xpi";
    sha256 = "5fe7ac089f9de8a10520813b527e76d95248aba8c8414e2dc2c359c505ca3b31";
    meta = with stdenv.lib; {
      homepage = "https://disconnect.me/";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "ecosia" = {
    pname = "ecosia";
    version = "5.1.1";
    addonId = "{d04b0b40-3dab-4f0b-97a6-04ec3eddbfb0}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4430040/ecosia_the_green_search-5.1.1.xpi";
    sha256 = "72308200cad2e6bd82847fa364a272d593a7592189a97331e1b55469f4d3650b";
    meta = with stdenv.lib; {
      homepage = "http://www.ecosia.org";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "facebook-container" = {
    pname = "facebook-container";
    version = "2.3.11";
    addonId = "@contain-facebook";
    url = "https://addons.mozilla.org/firefox/downloads/file/4141092/facebook_container-2.3.11.xpi";
    sha256 = "90dd562ffe0e6637791456558eabe083b0253e2b8a5df28f0ed0fdf1b7b175d0";
    meta = with stdenv.lib; {
      homepage = "https://github.com/mozilla/contain-facebook";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "firefox-color" = {
    pname = "firefox-color";
    version = "2.1.7";
    addonId = "FirefoxColor@mozilla.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/3643624/firefox_color-2.1.7.xpi";
    sha256 = "b7fb07b6788f7233dd6223e780e189b4c7b956c25c40493c28d7020493249292";
    meta = with stdenv.lib; {
      homepage = "https://color.firefox.com";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "foxyproxy-standard" = {
    pname = "foxyproxy-standard";
    version = "8.10";
    addonId = "foxyproxy@eric.h.jung";
    url = "https://addons.mozilla.org/firefox/downloads/file/4425860/foxyproxy_standard-8.10.xpi";
    sha256 = "80ab6ac87b8c8ef92b92f61dc3508e8ba42e4bd736ac03b3970e2a25457549dc";
    meta = with stdenv.lib; {
      homepage = "https://getfoxyproxy.org";
      license = licenses.gpl2;
      platforms = platforms.all;
    };
  };
  "fraidycat" = {
    pname = "fraidycat";
    version = "1.1.10";
    addonId = "{94060031-effe-4b93-89b4-9cd570217a8d}";
    url = "https://addons.mozilla.org/firefox/downloads/file/3783967/fraidycat-1.1.10.xpi";
    sha256 = "2bf975fef065290e19d5e70722021fc65e8499bececbdd56c616462d5bf834f1";
    meta = with stdenv.lib; {
      homepage = "https://fraidyc.at/";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "gesturefy" = {
    pname = "gesturefy";
    version = "3.2.13";
    addonId = "{506e023c-7f2b-40a3-8066-bc5deb40aebe}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4330901/gesturefy-3.2.13.xpi";
    sha256 = "f0908a53854e07fc436d053ea5528d180e7425fd3084ca0921cc558fce1a208b";
    meta = with stdenv.lib; {
      homepage = "https://github.com/Robbendebiene/Gesturefy";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "ghostery" = {
    pname = "ghostery";
    version = "10.4.27";
    addonId = "firefox@ghostery.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4449732/ghostery-10.4.27.xpi";
    sha256 = "317984e9671ccd4c41ca6ff4c2ac0254a3f5d551983a7d1cd86240c140e9e043";
    meta = with stdenv.lib; {
      homepage = "http://www.ghostery.com/";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "gopass-bridge" = {
    pname = "gopass-bridge";
    version = "2.1.0";
    addonId = "{eec37db0-22ad-4bf1-9068-5ae08df8c7e9}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4389774/gopass_bridge-2.1.0.xpi";
    sha256 = "ae2fe0296eea9ef73695bb57c52d9ba930c18d89d58d65c8014104cb8b0e74a1";
    meta = with stdenv.lib; {
      homepage = "https://github.com/gopasspw/gopassbridge";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "greasemonkey" = {
    pname = "greasemonkey";
    version = "4.13";
    addonId = "{e4a8a97b-f2ed-450b-b12d-ee082ba24781}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4332091/greasemonkey-4.13.xpi";
    sha256 = "31b9e9521eac579114ed20616851f4f984229fbe6d8ebd4dc4799eb48c59578c";
    meta = with stdenv.lib; {
      homepage = "http://www.greasespot.net/";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "swedish-dictionary" = {
    pname = "swedish-dictionary";
    version = "1.21";
    addonId = "swedish@dictionaries.addons.mozilla.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/3539390/gorans_hemmasnickrade_ordli-1.21.xpi";
    sha256 = "7d2ce7f7bfb65cfb5dd4138686acd977cf589c6ce91fc342ae5e2e26a09d1dbe";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.lgpl3;
      platforms = platforms.all;
    };
  };
  "header-editor" = {
    pname = "header-editor";
    version = "4.1.1";
    addonId = "headereditor-amo@addon.firefoxcn.net";
    url = "https://addons.mozilla.org/firefox/downloads/file/3472456/header_editor-4.1.1.xpi";
    sha256 = "389fba1a1a08b97f8b4bf0ed9c21ac2e966093ec43cecb80fc574997a0a99766";
    meta = with stdenv.lib; {
      homepage = "https://he.firefoxcn.net/en/";
      license = licenses.gpl2;
      platforms = platforms.all;
    };
  };
  "honey" = {
    pname = "honey";
    version = "12.8.4";
    addonId = "jid1-93CWPmRbVPjRQA@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/3731265/honey-12.8.4.xpi";
    sha256 = "1abc41b3d81879e8687696bb084ecceb40edec95ffa5b4516ad86185e13114cb";
    meta = with stdenv.lib; {
      homepage = "https://www.joinhoney.com";
      license = {
        shortName = "honeyl";
        fullName = "Custom License for Honey";
        url = "https://addons.mozilla.org/en-US/firefox/addon/honey/license/";
        free = false;
      };
      platforms = platforms.all;
    };
  };
  "i-dont-care-about-cookies" = {
    pname = "i-dont-care-about-cookies";
    version = "3.5.0";
    addonId = "jid1-KKzOGWgsW3Ao4Q@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4202634/i_dont_care_about_cookies-3.5.0.xpi";
    sha256 = "4de284454217fc4bee0744fb0aad8e0e10fa540dc03251013afc3ee4c20e49b0";
    meta = with stdenv.lib; {
      homepage = "https://www.i-dont-care-about-cookies.eu/";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "ipfs-companion" = {
    pname = "ipfs-companion";
    version = "3.2.0";
    addonId = "ipfs-firefox-addon@lidel.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/4447471/ipfs_companion-3.2.0.xpi";
    sha256 = "95b032a90f7e4349cb3778cdb9564b5c89d16c58d9aa12e2f223584ac143fb93";
    meta = with stdenv.lib; {
      homepage = "https://github.com/ipfs/ipfs-companion";
      license = licenses.cc0;
      platforms = platforms.all;
    };
  };
  "keepass-helper" = {
    pname = "keepass-helper";
    version = "1.4resigned1";
    addonId = "{e56fa932-ad2c-4cfa-b0d7-a35db1d9b0f6}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4273653/keepass_helper_url_in_title-1.4resigned1.xpi";
    sha256 = "8811193376573323610d0c1bd043c3977b6fa15fd804b37b0c4749aa9aed1fab";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "keepassxc-browser" = {
    pname = "keepassxc-browser";
    version = "1.9.7";
    addonId = "keepassxc-browser@keepassxc.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/4441759/keepassxc_browser-1.9.7.xpi";
    sha256 = "f4ecad9cabe70511fbff42fe4fe831c560cc3a0b6da10a740ce670f4f6597f42";
    meta = with stdenv.lib; {
      homepage = "https://keepassxc.org/";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "lastpass-password-manager" = {
    pname = "lastpass-password-manager";
    version = "4.138.3";
    addonId = "support@lastpass.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4417103/lastpass_password_manager-4.138.3.xpi";
    sha256 = "388153e8805db5def1827a2dfbcda3bb7b5f559dda49f49fd6ea509365ac8263";
    meta = with stdenv.lib; {
      homepage = "https://lastpass.com/";
      license = {
        shortName = "unfree";
        fullName = "Unfree";
        url = "https://addons.mozilla.org/en-US/firefox/addon/lastpass-password-manager/license/";
        free = false;
      };
      platforms = platforms.all;
    };
  };
  "leechblock-ng" = {
    pname = "leechblock-ng";
    version = "1.6.8.2";
    addonId = "leechblockng@proginosko.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4429788/leechblock_ng-1.6.8.2.xpi";
    sha256 = "c90c8f84e0ca5b574873f809b1cfe8b64c4377b10e52d6c59dd0ab53a9a02810";
    meta = with stdenv.lib; {
      homepage = "https://www.proginosko.com/leechblock/";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "link-cleaner" = {
    pname = "link-cleaner";
    version = "1.6resigned1";
    addonId = "{6d85dea2-0fb4-4de3-9f8c-264bce9a2296}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4272011/link_cleaner-1.6resigned1.xpi";
    sha256 = "16dbaf948c31ed586e64301d5809d7b11dd07014bf5edb5f7b1b4bfa30d40ff0";
    meta = with stdenv.lib; {
      homepage = "https://github.com/idlewan/link_cleaner";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "linkhints" = {
    pname = "linkhints";
    version = "1.3.3";
    addonId = "linkhints@lydell.github.io";
    url = "https://addons.mozilla.org/firefox/downloads/file/4346988/linkhints-1.3.3.xpi";
    sha256 = "209e50c4f9b9162d5ce0ebf4097518f51ae74129c29d920019497f6323871e6b";
    meta = with stdenv.lib; {
      homepage = "https://lydell.github.io/LinkHints";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "multi-account-containers" = {
    pname = "multi-account-containers";
    version = "8.2.0";
    addonId = "@testpilot-containers";
    url = "https://addons.mozilla.org/firefox/downloads/file/4355970/multi_account_containers-8.2.0.xpi";
    sha256 = "1ce35650853973572bc1ce770076d93e00b6b723b799f7b90c3045268c64b422";
    meta = with stdenv.lib; {
      homepage = "https://github.com/mozilla/multi-account-containers/#readme";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "old-reddit-redirect" = {
    pname = "old-reddit-redirect";
    version = "1.8.2";
    addonId = "{9063c2e9-e07c-4c2c-9646-cfe7ca8d0498}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4342347/old_reddit_redirect-1.8.2.xpi";
    sha256 = "0b89437d21132be520fbdf8f55a81cc41dd5f6b50c2905b4c43b870718baddf1";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "peertubeify" = {
    pname = "peertubeify";
    version = "0.6.1resigned1";
    addonId = "{01175c8e-4506-4263-bad9-d3ddfd4f5a5f}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4275922/peertubeify-0.6.1resigned1.xpi";
    sha256 = "d1752ebf0851b770be57c51e3d1dff7b61a69c12829dada885687c18d4f232b9";
    meta = with stdenv.lib; {
      homepage = "https://gitlab.com/Ealhad/peertubeify";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "plasma-integration" = {
    pname = "plasma-integration";
    version = "1.9.1";
    addonId = "plasma-browser-integration@kde.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/4298512/plasma_integration-1.9.1.xpi";
    sha256 = "394a3525185679dd5430d05f980ab6be19d96557560fe86208c21a8807669b33";
    meta = with stdenv.lib; {
      homepage = "http://kde.org";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "privacy-badger" = {
    pname = "privacy-badger";
    version = "2025.3.3";
    addonId = "jid1-MnnxcxisBPnSXQ@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4447530/privacy_badger17-2025.3.3.xpi";
    sha256 = "7acf878b3cd4a3b5ca7c98cbe3e31630bc6a048156db03bf2021795cce68ba97";
    meta = with stdenv.lib; {
      homepage = "https://privacybadger.org/";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "privacy-possum" = {
    pname = "privacy-possum";
    version = "2019.7.18";
    addonId = "woop-NoopscooPsnSXQ@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/3360398/privacy_possum-2019.7.18.xpi";
    sha256 = "0840a8c443e25d8a65da22ce1b557216456b900a699b3541e42e1b47e8cb6c0e";
    meta = with stdenv.lib; {
      homepage = "https://github.com/cowlicks/privacypossum";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "react-devtools" = {
    pname = "react-devtools";
    version = "6.0.0";
    addonId = "@react-devtools";
    url = "https://addons.mozilla.org/firefox/downloads/file/4360002/react_devtools-6.0.0.xpi";
    sha256 = "ed862b7bc65fc67a2cac53c3e4607ab9e54f4871656b048dcc42f49f613a0664";
    meta = with stdenv.lib; {
      homepage = "https://github.com/facebook/react";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "reddit-enhancement-suite" = {
    pname = "reddit-enhancement-suite";
    version = "5.24.8";
    addonId = "jid1-xUfzOsOFlzSOXg@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4424459/reddit_enhancement_suite-5.24.8.xpi";
    sha256 = "158405c50704a2cd2bc57c268a95b41dacba509b70d71d6ea280b04215bb8773";
    meta = with stdenv.lib; {
      homepage = "https://redditenhancementsuite.com/";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "reddit-moderator-toolbox" = {
    pname = "reddit-moderator-toolbox";
    version = "6.1.17";
    addonId = "yes@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4405464/reddit_moderator_toolbox-6.1.17.xpi";
    sha256 = "a0a08eaeabfa6b1f73d1b898e7bf749f671d54a4ced3f318db1e7a001c3eec3d";
    meta = with stdenv.lib; {
      homepage = "https://www.reddit.com/r/toolbox";
      license = licenses.asl20;
      platforms = platforms.all;
    };
  };
  "refined-github" = {
    pname = "refined-github";
    version = "25.2.26";
    addonId = "{a4c4eda4-fb84-4a84-b4a1-f7c1cbf2a1ad}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4444298/refined_github-25.2.26.xpi";
    sha256 = "447c9191fdd0d62eb47b696a7932f3eed69b1d4f9db547910cda61b3b604b2d2";
    meta = with stdenv.lib; {
      homepage = "https://github.com/refined-github/refined-github";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "save-page-we" = {
    pname = "save-page-we";
    version = "28.11";
    addonId = "savepage-we@DW-dev";
    url = "https://addons.mozilla.org/firefox/downloads/file/4091842/save_page_we-28.11.xpi";
    sha256 = "26f9504711fa44014f8cb57053d0900153b446e62308ccf1f3d989c287771cfd";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.gpl2;
      platforms = platforms.all;
    };
  };
  "sidebery" = {
    pname = "sidebery";
    version = "5.3.3";
    addonId = "{3c078156-979c-498b-8990-85f7987dd929}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4442132/sidebery-5.3.3.xpi";
    sha256 = "a4f9a8305f93b7d6b95f27943ecd1b3d422773fae5b802beac3af4a3e3a7476b";
    meta = with stdenv.lib; {
      homepage = "https://github.com/mbnuqw/sidebery";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "stylus" = {
    pname = "stylus";
    version = "2.3.13";
    addonId = "{7a7a4a92-a2a0-41d1-9fd7-1e92480d612d}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4439124/styl_us-2.3.13.xpi";
    sha256 = "e4ea2c09079db3d178b1d5f76e687534d2e35888f36bc2742767e6a63f161320";
    meta = with stdenv.lib; {
      homepage = "https://add0n.com/stylus.html";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "temporary-containers" = {
    pname = "temporary-containers";
    version = "1.9.2";
    addonId = "{c607c8df-14a7-4f28-894f-29e8722976af}";
    url = "https://addons.mozilla.org/firefox/downloads/file/3723251/temporary_containers-1.9.2.xpi";
    sha256 = "3340a08c29be7c83bd0fea3fc27fde71e4608a4532d932114b439aa690e7edc0";
    meta = with stdenv.lib; {
      homepage = "https://github.com/stoically/temporary-containers";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "text-contrast-for-dark-themes" = {
    pname = "text-contrast-for-dark-themes";
    version = "2.1.6";
    addonId = "jid1-nMVE2oP40qeQDQ@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/3462082/text_contrast_for_dark_themes-2.1.6.xpi";
    sha256 = "e768c13a4fa10e4dc2ce54f0539dd5a115c76babe6c044ae1115966f6062244d";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "textern" = {
    pname = "textern";
    version = "0.8";
    addonId = "textern@jlebon.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4123022/textern-0.8.xpi";
    sha256 = "e7c6dab734b44148ab10b1d3db016059d571bf7b0537c4f33c153e00327225ee";
    meta = with stdenv.lib; {
      homepage = "https://github.com/jlebon/textern";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "transparent-standalone-image" = {
    pname = "transparent-standalone-image";
    version = "2.2resigned1";
    addonId = "jid0-ezUl0hF1SPM9hLO5BMBkNoblB8s@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/4270447/transparent_standalone_image-2.2resigned1.xpi";
    sha256 = "bc7aeb16c9888c801300f818ff86737ca7e15315ab63c77acb309b42e2c0ba07";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mpl20;
      platforms = platforms.all;
    };
  };
  "tridactyl" = {
    pname = "tridactyl";
    version = "1.24.2";
    addonId = "tridactyl.vim@cmcaine.co.uk";
    url = "https://addons.mozilla.org/firefox/downloads/file/4405615/tridactyl_vim-1.24.2.xpi";
    sha256 = "807925f26aab56ab19a28e663ade73743a033e3b77aa09edd3f77bf92e5fb36e";
    meta = with stdenv.lib; {
      homepage = "";
      license = {
        shortName = "asl20";
        fullName = "";
        url = "";
        free = false;
      };
      platforms = platforms.all;
    };
  };
  "ublock-origin" = {
    pname = "ublock-origin";
    version = "1.62.0";
    addonId = "uBlock0@raymondhill.net";
    url = "https://addons.mozilla.org/firefox/downloads/file/4412673/ublock_origin-1.62.0.xpi";
    sha256 = "8a9e02aa838c302fb14e2b5bc88a6036d36358aadd6f95168a145af2018ef1a3";
    meta = with stdenv.lib; {
      homepage = "https://github.com/gorhill/uBlock#ublock-origin";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "umatrix" = {
    pname = "umatrix";
    version = "1.4.4";
    addonId = "uMatrix@raymondhill.net";
    url = "https://addons.mozilla.org/firefox/downloads/file/3812704/umatrix-1.4.4.xpi";
    sha256 = "1de172b1d82de28c334834f7b0eaece0b503f59e62cfc0ccf23222b8f2cb88e5";
    meta = with stdenv.lib; {
      homepage = "https://github.com/gorhill/uMatrix";
      license = licenses.gpl3;
      platforms = platforms.all;
    };
  };
  "vim-vixen" = {
    pname = "vim-vixen";
    version = "1.2.3";
    addonId = "vim-vixen@i-beam.org";
    url = "https://addons.mozilla.org/firefox/downloads/file/3845233/vim_vixen-1.2.3.xpi";
    sha256 = "8f86c77ac8e65dfd3f1a32690b56ce9231ac7686d5a86bf85e3d5cc5a3a9e9b5";
    meta = with stdenv.lib; {
      homepage = "https://github.com/ueokande/vim-vixen";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "vimium" = {
    pname = "vimium";
    version = "2.1.2";
    addonId = "{d7742d87-e61d-4b78-b8a1-b469842139fa}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4259790/vimium_ff-2.1.2.xpi";
    sha256 = "3b9d43ee277ff374e3b1153f97dc20cb06e654116a833674c79b43b8887820e1";
    meta = with stdenv.lib; {
      homepage = "https://github.com/philc/vimium";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "violentmonkey" = {
    pname = "violentmonkey";
    version = "2.30.0";
    addonId = "{aecec67f-0d10-4fa7-b7c7-609a2db280cf}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4426490/violentmonkey-2.30.0.xpi";
    sha256 = "82932f9d6ba68a230ef57b1cdcf9f76226b2659f94249eeec0e4ffcc2b644394";
    meta = with stdenv.lib; {
      homepage = "https://violentmonkey.github.io/";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "zoom-page-we" = {
    pname = "zoom-page-we";
    version = "19.13";
    addonId = "zoompage-we@DW-dev";
    url = "https://addons.mozilla.org/firefox/downloads/file/4098225/zoom_page_we-19.13.xpi";
    sha256 = "00ef2331283fdfe337fa26d65fee16b1c0409b240401aabf8cae6f56ef08d601";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.gpl2;
      platforms = platforms.all;
    };
  };
  "surfingkeys_ff" = {
    pname = "surfingkeys_ff";
    version = "1.17.5";
    addonId = "{a8332c60-5b6d-41ee-bfc8-e9bb331d34ad}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4394007/surfingkeys_ff-1.17.5.xpi";
    sha256 = "ca29f9edc16167f6234ebd22eb159e1e4640634142e34bdd27019b590b270a49";
    meta = with stdenv.lib; {
      homepage = "https://github.com/brookhong/Surfingkeys";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "sponsorblock" = {
    pname = "sponsorblock";
    version = "5.11.8";
    addonId = "sponsorBlocker@ajay.app";
    url = "https://addons.mozilla.org/firefox/downloads/file/4451103/sponsorblock-5.11.8.xpi";
    sha256 = "0706b77d29ce0517c046f55427510b3718f2efef36bb7c7953387089629f7ac0";
    meta = with stdenv.lib; {
      homepage = "https://sponsor.ajay.app";
      license = licenses.lgpl3;
      platforms = platforms.all;
    };
  };
  "h264ify" = {
    pname = "h264ify";
    version = "1.1.0";
    addonId = "jid1-TSgSxBhncsPBWQ@jetpack";
    url = "https://addons.mozilla.org/firefox/downloads/file/3398929/h264ify-1.1.0.xpi";
    sha256 = "87bd3c4ab1a2359c01a1d854d7db8428b44316fef5b2ac09e228c5318c57a515";
    meta = with stdenv.lib; {
      homepage = "";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
  "kagi-search-for-firefox" = {
    pname = "kagi-search-for-firefox";
    version = "0.7.6";
    addonId = "search@kagi.com";
    url = "https://addons.mozilla.org/firefox/downloads/file/4429158/kagi_search_for_firefox-0.7.6.xpi";
    sha256 = "51cac0f2f57e3d7671d502df66a6019a8ed3a280e690249f09dcda0fb570990f";
    meta = with stdenv.lib; {
      homepage = "https://kagi.com";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
}