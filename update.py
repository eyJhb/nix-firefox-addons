#!/usr/bin/env nix-shell
#!nix-shell --pure -i python3 -p "python3.withPackages (ps: with ps; [ requests jinja2 ])"
from typing import Any, Optional
import enum
import requests
import logging
import json
from jinja2 import Template


logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

FILE_ADDONS = "addons.json"
OUT_FILE = "default.nix"
URL_BASE = "https://addons.mozilla.org/api/v4/addons/addon/"

FIREFOX_ADDON_LICENSE_MAPPING = {
    6: "gpl3",
    12: "lgpl3",
    13: "gpl2",
    22: "mit",
    3338: "mpl20",
    4160: "asl20",
    4814: "cc0",
    5296: "bsd3",
    7551: "isc",
    7552: "isc",
    7770: "agpl3",
}

NIX_TEMPLATE = Template(
    """{ stdenv }:

{
{%- for addon in addons if addon.license or addon.custom_license %}
  "{{ addon.pname }}" = {
    pname = "{{ addon.pname }}";
    version = "{{ addon.version }}";
    addonId = "{{ addon.guid }}";
    url = "{{ addon.url }}";
    sha256 = "{{ addon.sha256 }}";
    meta = with stdenv.lib; {
      homepage = "{{ addon.homepage }}";
      {%- if addon.custom_license %}
      license = {
        shortName = "{{ addon.custom_license.shortName }}";
        fullName = "{{ addon.custom_license.fullName }}";
        url = "{{ addon.custom_license.url }}";
        free = {{ addon.custom_license.free or false | lower }};
      };
      {%- else %}
      license = licenses.{{ addon.license }};
      {%- endif %}
      platforms = platforms.all;
    };
  };
{%- endfor %}
}
"""
)


class FirefoxAddon:
    def __init__(
        self,
        obj: dict[str, Any],
        pname: Optional[str] = None,
        custom_license: Optional[dict[str, Any]] = None,
    ):
        self.pname: str = pname if pname else obj["slug"]
        self.guid: str = obj["guid"]
        self.author: str = obj["authors"][0]["name"]
        self.homepage: str = self.tryGet(obj["homepage"], "en-US")
        self.summary: str = self.tryGet(obj["summary"], "en-US")
        self.version: str = obj["current_version"]["version"]
        self.url: str = obj["current_version"]["files"][0]["url"]
        self.sha256: str = obj["current_version"]["files"][0]["hash"][7:]

        # setup the correct license
        self._tmp_license = obj["current_version"]["license"]["id"]
        self.license = (
            FIREFOX_ADDON_LICENSE_MAPPING[self._tmp_license]
            if self._tmp_license in FIREFOX_ADDON_LICENSE_MAPPING
            else ""
        )

        # maybe there is a custom license
        self.custom_license: Optional[dict[str, Any]] = custom_license

    def tryGet(self, obj: dict[str, Any], key: str) -> str:
        if not obj:
            return ""

        try:
            return obj[key]
        except KeyError:
            return ""


class FirefoxAddonsToNix:
    def __init__(self):
        self._addons = self._fetch_firefox_addons()

    def _fetch_firefox_addons(self) -> list[FirefoxAddon]:
        with open(FILE_ADDONS, "r") as f:
            raw_addons = json.loads(f.read())

        addons: list[FirefoxAddon] = []
        for raw_addon in raw_addons:
            raw_addon_data = self._fetch_raw_addon(raw_addon["slug"])
            if not raw_addon_data:
                log.error(f"failed to fetch addon: {raw_addon}")
                continue

            addons.append(
                FirefoxAddon(
                    obj=raw_addon_data,
                    pname=raw_addon.get("pname"),
                    custom_license=raw_addon.get("license"),
                )
            )

        return addons

    def _fetch_raw_addon(self, addon_slug: str) -> Optional[dict[str, Any]]:
        url = URL_BASE + addon_slug
        req = requests.get(url)
        if req.status_code != 200:
            return None

        return req.json()

    def write_nix_file(self):
        with open(OUT_FILE, "w") as f:
            f.write(NIX_TEMPLATE.render(addons=self._addons))


if __name__ == "__main__":
    x = FirefoxAddonsToNix()
    x.write_nix_file()
